﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Dependency_Inversion_Principle.Interface
{
    public interface IRelationshipBrowser
    {
        IEnumerable<Person> FindChildrenOf(string name);
    }
}
