﻿using DesignPatterns.Dependency_Inversion_Principle.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Dependency_Inversion_Principle
{
    public class Relation
    {
        public Person Person1 { get; set; }

        public Person Person2 { get; set; }

        public Relationship Relationship { get; set; }
    }
}
