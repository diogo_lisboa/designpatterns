﻿using DesignPatterns.Dependency_Inversion_Principle.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Dependency_Inversion_Principle
{
    /// <summary>
    /// This principle allows our high-level modules to be independent of our low-level modules.
    /// By creation an abstraction on our low-level module, we assure that it can be modified later on
    /// without affecting our high-level module because it isn't directly consuming the low-level anymore.
    /// </summary>
    public static class DependencyInversionPrinciple
    {
        public static void RunPrinciple()
        {
            var parent = new Person() { Name = "Tânia" };
            var child1 = new Person() { Name = "Diogo" };
            var child2 = new Person() { Name = "Anna" };
            var child3 = new Person() { Name = "Marcos" };

            var relationships = new Relationships();
            relationships.AddParentAndChild(parent, child1);
            relationships.AddParentAndChild(parent, child2);
            relationships.AddParentAndChild(parent, child3);

            new Research(relationships);
        }
    }
}
