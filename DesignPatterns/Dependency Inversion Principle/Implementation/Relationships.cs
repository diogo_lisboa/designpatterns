﻿using DesignPatterns.Dependency_Inversion_Principle.Enums;
using DesignPatterns.Dependency_Inversion_Principle.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Dependency_Inversion_Principle.Implementation
{
    public class Relationships : IRelationshipBrowser
    {
        private List<Relation> relations = new List<Relation>();

        public void AddParentAndChild(Person parent, Person child)
        {
            relations.Add(new Relation()
            {
                Person1 = parent,
                Person2 = child,
                Relationship = Relationship.Parent
            });

            relations.Add(new Relation()
            {
                Person1 = child,
                Person2 = parent,
                Relationship = Relationship.Child
            });
        }

        public IEnumerable<Person> FindChildrenOf(string name)
        {
            return relations.Where(
                x => x.Person1.Name.Equals(name) &&
                x.Relationship == Relationship.Parent
            ).Select(x => x.Person2);
        }
    }
}
