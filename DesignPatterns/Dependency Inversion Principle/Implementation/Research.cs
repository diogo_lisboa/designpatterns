﻿using DesignPatterns.Dependency_Inversion_Principle.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Dependency_Inversion_Principle.Implementation
{
    public class Research
    {
        public Research(IRelationshipBrowser browser)
        {
            foreach (var p in browser.FindChildrenOf("Tânia"))
                Console.WriteLine($"Tânia has child {p.Name}.", Environment.NewLine);
        }
    }
}
