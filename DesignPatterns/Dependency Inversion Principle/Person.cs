﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Dependency_Inversion_Principle
{
    public class Person
    {
        public string Name { get; set; }
    }
}
