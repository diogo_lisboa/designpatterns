﻿using DesignPatterns.Dependency_Inversion_Principle;
using DesignPatterns.Open_Closed_Principle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns
{
    class Program
    {
        static void Main(string[] args)
        {
            //OpenClosedPrinciple.RunPrinciple();

            DependencyInversionPrinciple.RunPrinciple();

            Console.ReadLine();
        }
    }
}
