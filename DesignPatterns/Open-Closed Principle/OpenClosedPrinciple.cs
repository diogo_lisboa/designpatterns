﻿using DesignPatterns.Open_Closed_Principle.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static DesignPatterns.Open_Closed_Principle.Enums.Enums;

namespace DesignPatterns.Open_Closed_Principle
{
    /// <summary>
    /// This principle states that entities should be open for extensions and not for modifications,
    /// this way preserving the source code.
    /// </summary>
    public static class OpenClosedPrinciple
    {
        public static void RunPrinciple()
        {
            Product[] products = {
                new Product("Apple", Size.Small, Color.Red),
                new Product("Orange", Size.Small, Color.Orange),
                new Product("Pineapple", Size.Medium, Color.Yellow),
                new Product("Watermelon", Size.Large, Color.Green),
                new Product("Huge Apple", Size.Large, Color.Red)
            };

            Console.WriteLine("Products: ", Environment.NewLine);
            foreach (var i in products)
                Console.WriteLine($"NAME: {i.Name} \t SIZE {i.Size} \t COLOR: {i.Color}");
            

            var pf = new ProductFilter();

            //Filtering by color, ColorSpecification implements the ISpecification interface
            Console.WriteLine(Environment.NewLine, Environment.NewLine);
            Console.WriteLine("Red Products: ", Environment.NewLine);
            foreach (var i in  pf.Filter(products, new ColorSpecification(Color.Red)))
                Console.WriteLine($"NAME: {i.Name} \t SIZE {i.Size} \t COLOR: {i.Color}");
            
            //Filtering by size, SizeSpecification implements the ISpecification interface
            Console.WriteLine(Environment.NewLine, Environment.NewLine);
            Console.WriteLine("Small Products: ", Environment.NewLine);
            foreach (var i in pf.Filter(products, new SizeSpecification(Size.Small)))
                Console.WriteLine($"NAME: {i.Name} \t SIZE {i.Size} \t COLOR: {i.Color}");

            //Filtering by color and size, AndSpecification implements the ISpecification interface
            Console.WriteLine(Environment.NewLine, Environment.NewLine);
            Console.WriteLine("Large Red Products: ", Environment.NewLine);
            foreach (var i in pf.Filter(products, new AndSpecification<Product>(
                new ColorSpecification(Color.Red),
                new SizeSpecification(Size.Large)
                )))
                Console.WriteLine($"NAME: {i.Name} \t SIZE {i.Size} \t COLOR: {i.Color}");

            //Above we added 3 possible ways of filtering the entities without modifications to the source code,
            //and only extending it
        }
    }
}
