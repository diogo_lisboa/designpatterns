﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Open_Closed_Principle.Enums
{
    public class Enums
    {
        public enum Color
        {
            Red, Blue, White, Black, Orange, Yellow,
            Green
        }

        public enum Size
        {
            Small, Medium, Large
        }
    }
}
