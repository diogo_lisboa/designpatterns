﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static DesignPatterns.Open_Closed_Principle.Enums.Enums;

namespace DesignPatterns.Open_Closed_Principle
{
    public class Product
    {
        public string Name { get; set; }

        public Size Size { get; set; }

        public Color Color { get; set; }


        public Product(string name, Size size, Color color)
        {
            this.Name = name;
            this.Size = size;
            this.Color = color;
        }
    }
}
